<?php

namespace Framework\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

class ControllerEvent extends Event
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var callable
     */
    protected $controller;

    /**
     * ControllerEvent constructor.
     * @param Request $request
     * @param callable $controller
     */
    public function __construct(Request $request, callable $controller)
    {
        $this->request = $request;
        $this->controller = $controller;
    }

    /**
     * @return callable
     */
    public function getController(): callable
    {
        return $this->controller;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }
}
