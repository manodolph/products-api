<?php

namespace Framework\Event;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\Event;

class ArgumentEvent extends Event
{
    protected $request;
    protected $controller;
    protected $arguments;

    /**
     * ArgumentEvent constructor.
     * @param Request $request
     * @param callable $controller
     * @param array $arguments
     */
    public function __construct(Request $request, callable $controller, array $arguments)
    {
        $this->request = $request;
        $this->controller = $controller;
        $this->arguments = $arguments;
    }

    /**
     * @return callable
     */
    public function getController(): callable
    {
        return $this->controller;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }
}
