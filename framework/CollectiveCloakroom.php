<?php

namespace Framework;

use Exception;
use Framework\Event\ArgumentEvent;
use Framework\Event\ControllerEvent;
use Framework\Event\RequestEvent;
use Framework\Event\ResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CollectiveCloakroom
{
    /**
     * @var UrlMatcherInterface
     */
    protected  $urlMatcher;
    /**
     * @var ControllerResolverInterface
     */
    protected  $controllerResolver;
    /**
     * @var ArgumentResolverInterface
     */
    protected  $argumentResolver;
    /**
     * @var EventDispatcherInterface
     */
    protected  $dispatcher;

    /**
     * CollectiveCloakroom constructor.
     * @param EventDispatcherInterface $dispatcher
     * @param UrlMatcherInterface $urlMatcher
     * @param ControllerResolverInterface $controllerResolver
     * @param ArgumentResolverInterface $argumentResolver
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        UrlMatcherInterface $urlMatcher,
        ControllerResolverInterface $controllerResolver,
        ArgumentResolverInterface $argumentResolver
    ) {
        $this->dispatcher = $dispatcher;
        $this->urlMatcher = $urlMatcher;
        $this->controllerResolver = $controllerResolver;
        $this->argumentResolver = $argumentResolver;
    }

    /**
     * @param Request $request
     * @return false|mixed|Response
     */
    public function handle(Request $request)
    {
        $this->urlMatcher->getContext()->fromRequest($request);

        try {
            $request->attributes->add($this->urlMatcher->match($request->getPathInfo()));

            $this->dispatcher->dispatch(new RequestEvent($request), 'kernel.request');

            $controller = $this->controllerResolver->getController($request);

            $this->dispatcher->dispatch(new ControllerEvent($request, $controller), 'kernel.controller');

            $arguments = $this->argumentResolver->getArguments($request, $controller);

            $this->dispatcher->dispatch(new ArgumentEvent($request, $controller, $arguments), 'kernel.arguments');

            $response = call_user_func_array($controller, $arguments);

            $this->dispatcher->dispatch(new ResponseEvent($response), 'kernel.response');
        } catch (ResourceNotFoundException $e) {
            $response = new Response("Sorry, this page does not exist.", 404);
        } catch (Exception $e) {
            $response = new Response("Sorry, there is an error. ", 500);
        }

        return $response;
    }
}
