<?php

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routes = new RouteCollection();

$routes->add('product', new Route('/product/{id}', [
    'id' => null,
    '_controller' => 'App\Controller\ApiController::product'
]));

$routes->add('products', new Route('/products', [
    '_controller' => 'App\Controller\ApiController::products'
]));

$routes->add('product_search', new Route('/product-search', [
    '_controller' => 'App\Controller\ApiController::productSearch'
]));

$routes->add('install', new Route('/install', [
    '_controller' => 'App\Controller\ApiController::productsInstall'
]));

$routes->add('page.about', new Route('/a-propos', [
    '_controller' => 'App\Controller\PageController::about'
]));

return $routes;
