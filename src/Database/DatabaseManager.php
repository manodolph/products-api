<?php

namespace App\Database;

use SleekDB\Query;

class DatabaseManager
{
    protected $storeName;
    protected $store;
    protected $databaseName;

    /**
     * DatabaseManager constructor.
     * @param $storeName
     * @param $databaseName
     * @throws \SleekDB\Exceptions\IOException
     * @throws \SleekDB\Exceptions\InvalidArgumentException
     * @throws \SleekDB\Exceptions\InvalidConfigurationException
     */
    public function __construct($storeName, $databaseName)
    {
        $this->storeName    = $storeName;
        $this->databaseName = $databaseName;
        $databaseDirectory  = __DIR__ . "/../../storage/databases/" . $databaseName;

        $configuration = [
            "search" => [
                "min_length" => 2,
                "mode" => "or",
                "score_key" => "scoreKey",
                "algorithm" => Query::SEARCH_ALGORITHM["hits"]
            ]
        ];

        $this->store = new \SleekDB\Store($storeName, $databaseDirectory, $configuration);
    }

    /**
     * @return \SleekDB\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * @throws \SleekDB\Exceptions\IOException
     * @throws \SleekDB\Exceptions\IdNotAllowedException
     * @throws \SleekDB\Exceptions\InvalidArgumentException
     * @throws \SleekDB\Exceptions\JsonException
     */
    public function installStore()
    {
        $products = require  __DIR__ . '/../Database/store.php';
        foreach ($products as $product) {
            $this->store->insert($product);
        }
    }

    /**
     * @throws \SleekDB\Exceptions\IOException
     */
    public function deleteStore()
    {
        $this->store->deleteStore();
    }
}
