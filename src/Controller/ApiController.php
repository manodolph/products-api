<?php

namespace App\Controller;

use App\Database\DatabaseManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Model\Product;

class ApiController
{
    /**
     * @return JsonResponse
     */
    public function products()
    {
        $product = new Product();
        $products = $product->getAll();
        $response = new JsonResponse($products);
        $response->setEncodingOptions(JSON_PRETTY_PRINT);

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function product($id, Request $request)
    {
        $id = $request->attributes->get('id');
        $product = new Product();
        $productData = $product->getProduct($id);

        $response = new JsonResponse($productData);
        $response->setEncodingOptions(JSON_PRETTY_PRINT);

        return $response;
    }

    /**
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function productSearch(Request $request)
    {
        $productData = [];
        $searchString = $request->query->get("search");

        if ($searchString) {
            $product = new Product();
            $productData = $product->searchProduct($searchString);
        }



        $response = new JsonResponse($productData);
        $response->setEncodingOptions(JSON_PRETTY_PRINT);

        return $response;
    }


    /**
     * @return Response
     * @throws \SleekDB\Exceptions\IOException
     * @throws \SleekDB\Exceptions\IdNotAllowedException
     * @throws \SleekDB\Exceptions\InvalidArgumentException
     * @throws \SleekDB\Exceptions\JsonException
     */
    public function productsInstall()
    {
        $dbManager = new DatabaseManager('productsStore', 'products_db');
        //$dbManager->deleteStore();
        $dbManager->installStore();
        return new Response("done", 200);
    }
}
