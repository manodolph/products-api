<?php

namespace App\Model;

use App\Database\DatabaseManager;
use phpDocumentor\Reflection\Types\This;
use SleekDB\Query;

class Product
{
    /**
     * @return array
     * @throws \SleekDB\Exceptions\IOException
     * @throws \SleekDB\Exceptions\InvalidArgumentException
     */
    public function getAll()
    {
        $store = $this->getStore();
        return $store->findAll(["name" => "asc"]);
    }


    /**
     * @param $id
     * @return array
     * @throws \SleekDB\Exceptions\IOException
     * @throws \SleekDB\Exceptions\InvalidArgumentException
     */
    public function getProduct($id)
    {
        $store = $this->getStore();
        return $store->findBy(["id", "=", $id]);
    }

    /**
     * @param $search
     * @return array
     * @throws \SleekDB\Exceptions\IOException
     * @throws \SleekDB\Exceptions\InvalidArgumentException
     */
    public function searchProduct($search)
    {
        $searchOptions = [
            "minLength" => 2,
            "mode" => "or",
            "scoreKey" => "scoreKey",
            "algorithm" => Query::SEARCH_ALGORITHM["hits"]
        ];

        $store = $this->getStore();

        return $store->createQueryBuilder()
            ->search(["name"], $search, $searchOptions)
            ->getQuery()
            ->fetch();
    }

    /**
     * @return \SleekDB\Store
     */
    private function getStore()
    {
        $dbManager = new DatabaseManager('productsStore', 'products_db');
        return $dbManager->getStore();
    }
}
