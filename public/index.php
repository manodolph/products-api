<?php
/*
 * Based on front controller design pattern
 *
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

use Framework\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Contracts\EventDispatcher\Event;

//get classes managed by composer
require __DIR__ . '/../vendor/autoload.php';

$request = Request::createFromGlobals();

//gte configured routes array
$routes = require __DIR__ . '/../src/routes.php';

//Holds information about the current request
$context = new RequestContext();

//matches URL based on a set of routes
$urlMatcher = new UrlMatcher($routes, $context);


//determines the controller to execute.
$controllerResolver = new ControllerResolver();

//resolves the arguments passed to an action
$argumentResolver = new ArgumentResolver();

$dispatcher = new EventDispatcher;

$framework = new Framework\CollectiveCloakroom($dispatcher, $urlMatcher, $controllerResolver, $argumentResolver);

$response = $framework->handle($request);

$response->send();
