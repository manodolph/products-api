# Products-api
## _based on solid design patterns_

## Features

- GET /products endpoint returning all the products.
- GET /product/{id} endpoint to return a single product.
- GET /product-search?search=[NAME] endpoint to search products.


## Tech
- PHP
- Composer
- JSON
- SleekDB - NoSQL Database

## Installation

```sh
cd products-api
composer install #to install dependencies
cd public && php -S localhost:8080 #to run internal php server
```

install products using the url [api url]/install

## PHP UNIT

```sh
vendor/bin/phpunit   tests/ProductTest.php
```

## DEMO
All products
https://productsapi.lightapps.tn/products

Get product by Id
https://productsapi.lightapps.tn/product/9

Product search
https://productsapi.lightapps.tn/product-search?search=Cotton
