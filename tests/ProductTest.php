<?php

use Framework\CollectiveCloakroom;
use PHPUnit\Framework\ExpectationFailedException;
use PHPUnit\Framework\TestCase;
use SebastianBergmann\RecursionContext\InvalidArgumentException;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;

class ProductTest extends TestCase
{

    protected  $framework;

    protected function setUp(): void
    {
        $routes = require __DIR__ . '/../src/routes.php';

        $urlMatcher = new UrlMatcher($routes, new RequestContext());
        $controllerResolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();
        $dispatcher = new EventDispatcher;

        $this->framework = new CollectiveCloakroom($dispatcher, $urlMatcher, $controllerResolver, $argumentResolver);
    }

    public function testProduct()
    {
        $request = Request::create('/product/1');

        $response = $this->framework->handle($request);
        $expectedName = "Alice cloth backpack";
        $responseObject = json_decode($response->getContent());

        $this->assertEquals($expectedName, $responseObject->name);
    }

}
